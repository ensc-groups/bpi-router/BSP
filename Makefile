top_srcdir :=		$(dir $(abspath $(firstword $(MAKEFILE_LIST))))

PROJECT =		bpi-router
SETUP =			default

MACHINE =		bananapi-m2b

DEPLOY_DIR ?=		${BUILDDIR}/tmp/default/deploy

OECORE_DIR =		$(top_srcdir)/sources/org.openembedded.core
META_SIGMA_DIR =	sources/de.sigma-chemnitz.core
BB_ENV_EXTRAWHITE =	BB_GENERATE_MIRROR_TARBALLS

include ${META_SIGMA_DIR}/mk/defs.mk
-include Makefile.extra

ifeq (${BUILDMODE},.ci)

tmpl.conf:
	rm -f $@
	@echo 'LICENSE_FLAGS_ACCEPTED += "commercial_gstreamer1.0-libav"'  >> $@
	@echo 'LICENSE_FLAGS_ACCEPTED += "commercial_faad2"'  >> $@
	@echo 'ACCEPT_FSL_EULA = "1"'  >> $@
	@echo "require $${HOME}/.config/openembedded/bitbake.conf" >> $@

endif				# BUILDMODE == ci
