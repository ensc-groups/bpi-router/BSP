* BPI M1

** Network

#+BEGIN_SRC
# iperf3 -4 -t 30 -c delenn
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-30.00  sec  1.80 GBytes   514 Mbits/sec    0             sender
[  5]   0.00-30.04  sec  1.80 GBytes   513 Mbits/sec                  receiver
#+END_SRC

#+BEGIN_SRC
# iperf3 -4 -t 30 -c delenn -R 
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-30.04  sec  3.02 GBytes   863 Mbits/sec  143             sender
[  5]   0.00-30.00  sec  3.01 GBytes   863 Mbits/sec                  receiver
#+END_SRC


** Crypto

*** default engine

#+BEGIN_SRC
# openssl speed md5 sha1 sha256 sha512 des des-ede3 aes-128-cbc aes-192-cbc aes-256-cbc rsa2048 dsa2048
OpenSSL 1.1.1a  20 Nov 2018
built on: Sun Feb 17 12:36:18 2019 UTC
options:bn(64,32) rc4(char) des(long) aes(partial) idea(int) blowfish(ptr)
compiler: arm-oe-linux-gnueabi-gcc  -march=armv7-a -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -fstack-protector-strong  -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=recipe-sysroot -O2 -pipe -g -feliminate-unused-debug-types -fdebug-prefix-map= -fdebug-prefix-map= -fdebug-prefix-map= -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG
#+END_SRC

#+BEGIN_SRC sh
openssl speed -evp aes-128-gcm
openssl speed -evp aes-128-ctr
#+END_SRC

 | type        |     16 bytes |     64 bytes |    256 bytes |   1024 bytes |   8192 bytes |  16384 bytes |
 |             |        <r12> |        <r12> |        <r12> |        <r12> |        <r12> |        <r12> |
 |-------------+--------------+--------------+--------------+--------------+--------------+--------------|
 | md5         |    13204.18k |    36535.72k |    74863.65k |   101511.62k |   113175.21k |   113928.87k |
 | sha1        |     9289.80k |    22790.95k |    43562.72k |    55680.09k |    61282.25k |    61586.19k |
 | des cbc     |    11067.76k |    11902.27k |    12097.90k |    12220.90k |    12147.24k |    12170.19k |
 | des ede3    |     4101.79k |     4230.49k |     4255.40k |     4262.23k |     4260.39k |     4258.74k |
 | aes-128 cbc |    21877.03k |    24048.18k |    24576.86k |    24825.18k |    24710.66k |    24822.88k |
 | aes-192 cbc |    18738.21k |    20456.01k |    21017.86k |    21079.04k |    21113.51k |    21108.05k |
 | aes-256 cbc |    16834.38k |    18040.27k |    18550.41k |    18585.60k |    18517.28k |    18492.94k |
 | sha256      |     5823.75k |    13591.25k |    24790.97k |    30997.85k |    33494.36k |    33821.65k |
 | sha512      |     3148.57k |    12486.62k |    18910.32k |    26318.17k |    29791.57k |    30048.26k |
 |-------------+--------------+--------------+--------------+--------------+--------------+--------------|
 | aes-128-gcm |    12750.94k |    16136.03k |    16803.84k |    17942.07k |    18175.32k |    18169.86k |
 | aes-128-ctr |    16943.60k |    22791.66k |    24604.25k |    26826.75k |    27541.50k |    27611.70k |

 |               | sign      | verify    | sign/s | verify/s |
 |---------------+-----------+-----------+--------+----------|
 | rsa 2048 bits | 0.023966s | 0.000565s |   41.7 |   1768.4 |
 | dsa 2048 bits | 0.007498s | 0.006519s |  133.4 |    153.4 |


*** TODO afalg engine

#+BEGIN_SRC
# openssl engine -c afalg
(afalg) AFALG engine support
 [AES-128-CBC, AES-192-CBC, AES-256-CBC]
#+END_SRC

#+BEGIN_SRC sh
openssl speed -engine afalg AES-128-CBC

#+END_SRC

--> Test is broken; bad time:

#+BEGIN_SRC
Doing aes-256-cbc for 3s on 16384 size blocks: 4924 aes-256-cbc's in 0.00s
#+END_SRC


* BPI M2B

** Network

#+BEGIN_SRC
# iperf3 -4 -t 30 -c delenn
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-30.00  sec  2.92 GBytes   836 Mbits/sec    0             sender
[  5]   0.00-30.04  sec  2.92 GBytes   833 Mbits/sec                  receiver
#+END_SRC

#+BEGIN_SRC
# iperf3 -c  delenn -R
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-30.05  sec  3.22 GBytes   921 Mbits/sec  1056             sender
[  5]   0.00-30.00  sec  3.22 GBytes   922 Mbits/sec                  receiver
#+END_SRC


** crypto

*** default engine

#+BEGIN_SRC 
# openssl speed md5 sha1 sha256 sha512 des des-ede3 aes-128-cbc aes-192-cbc aes-256-cbc rsa2048 dsa2048

compiler: arm-oe-linux-gnueabi-gcc  -march=armv7-a -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -fstack-protector-strong  -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=recipe-sysroot -O2 -pipe -g -feliminate-unused-debug-types -fmacro-prefix-map=                      -fdebug-prefix-map=                      -fdebug-prefix-map=                      -fdebug-prefix-map= -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG
The 'numbers' are in 1000s of bytes per second processed.
type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes
md5              14092.79k    37776.33k    77462.45k   104970.24k   117394.09k   117915.81k
sha1              9717.29k    23929.88k    45530.49k    58746.30k    64441.00k    64703.19k
des cbc          11439.56k    12418.85k    12724.82k    12751.69k    12777.89k    12817.75k
des ede3          4370.24k     4501.91k     4539.65k     4545.88k     4534.18k     4524.59k
aes-128 cbc      23255.35k    25315.24k    26054.98k    26081.72k    26130.03k    26116.42k
aes-192 cbc      19887.67k    21548.18k    22076.64k    22198.69k    22238.15k    22240.87k
aes-256 cbc      17765.21k    19139.48k    19479.73k    19576.09k    19614.53k    19677.18k
sha256            6127.89k    14395.27k    26037.67k    32603.34k    35356.67k    35553.28k
sha512            3320.44k    13286.14k    19819.25k    27668.41k    31317.39k    31692.12k
                  sign    verify    sign/s verify/s
rsa 2048 bits 0.022244s 0.000540s     45.0   1852.8
                  sign    verify    sign/s verify/s
dsa 2048 bits 0.007090s 0.006323s    141.0    158.1
#+END_SRC

*** TODO afalg engine

#+BEGIN_SRC
# openssl engine -c afalg
(afalg) AFALG engine support
 [AES-128-CBC, AES-192-CBC, AES-256-CBC]
#+END_SRC

#+BEGIN_SRC sh
OpenSSL 1.1.1d  10 Sep 2019
built on: Fri Dec 27 15:20:49 2019 UTC
options:bn(64,32) rc4(char) des(long) aes(partial) idea(int) blowfish(ptr) 
compiler: arm-oe-linux-gnueabi-gcc  -march=armv7-a -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -fstack-protector-strong  -D_FORTIFY_SOURCE=2 -Wformat -Wformat-security -Werror=format-security --sysroot=recipe-sysroot -O2 -pipe -g -feliminate-unused-debug-types -fmacro-prefix-map=                      -fdebug-prefix-map=                      -fdebug-prefix-map=                      -fdebug-prefix-map= -DOPENSSL_USE_NODELETE -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG
The 'numbers' are in 1000s of bytes per second processed.
type             16 bytes     64 bytes    256 bytes   1024 bytes   8192 bytes  16384 bytes
md2                  0.00         0.00         0.00         0.00         0.00         0.00 
mdc2              1788.91k     2113.30k     2215.80k     2239.87k     2250.76k     2246.02k
md4               5991.99k    21364.20k    59000.52k   105896.57k   137933.14k   140978.60k
md5              14049.98k    37939.90k    77617.67k   104854.54k   117107.50k   118117.21k
hmac(md5)         5193.30k    17460.39k    48722.75k    87324.75k   113955.89k   116478.81k
sha1              9696.35k    23883.80k    45538.66k    58829.65k    64368.44k    64801.17k
rmd160            4087.46k    11719.59k    24791.43k    34411.84k    38801.78k    39141.97k
rc4              47328.56k    54501.29k    56656.29k    57202.82k    57390.27k    57398.43k
des cbc          11449.20k    12429.37k    12698.88k    12767.68k    12788.77k    12791.50k
des ede3          4363.55k     4496.16k     4532.99k     4542.34k     4545.06k     4545.06k
idea cbc         14360.12k    15828.58k    16262.97k    16375.15k    16408.49k    16411.22k
seed cbc         16446.10k    18065.44k    18567.14k    18697.02k    18735.46k    18735.46k
rc2 cbc          10235.67k    10860.95k    11027.99k    11071.11k    11085.06k    11082.33k
rc5-32/12 cbc        0.00         0.00         0.00         0.00         0.00         0.00 
blowfish cbc     21956.07k    26055.85k    27333.91k    27668.07k    27773.87k    27782.04k
cast cbc         18928.67k    21971.43k    22896.61k    23141.72k    23212.48k    23220.65k
aes-128 cbc      23119.61k    25223.23k    25892.06k    26065.39k    26113.70k    26029.94k
aes-192 cbc      19913.34k    21526.67k    22051.30k    22185.76k    22224.54k    22229.99k
aes-256 cbc      17727.39k    19057.52k    19466.89k    19572.69k    19603.65k    19606.37k
camellia-128 cbc    19119.04k    21410.83k    22154.38k    22351.44k    22409.61k    22409.61k
camellia-192 cbc    15461.20k    16927.96k    17381.80k    17498.83k    17535.23k    17532.51k
camellia-256 cbc    15402.75k    16910.80k    17376.70k    17496.45k    17532.51k    17537.96k
sha256            6133.99k    14325.01k    25986.98k    32637.02k    35266.42k    35467.82k
sha512            3312.73k    13266.67k    19862.28k    27699.03k    31339.16k    31635.82k
whirlpool         1120.55k     2311.82k     3774.68k     4485.87k     4746.46k     4768.23k
aes-128 ige      20089.66k    23590.91k    24653.65k    24933.89k    25016.90k    24973.35k
aes-192 ige      17870.49k    20262.70k    21003.74k    21197.48k    21255.65k    21217.55k
aes-256 ige      16132.73k    18060.40k    18646.84k    18799.76k    18844.32k    18817.11k
ghash            45522.64k    53592.92k    56075.65k    56710.21k    56924.87k    56930.32k
rand               519.08k     1902.90k     5699.22k    11484.32k    16245.20k    16765.02k
                  sign    verify    sign/s verify/s
rsa  512 bits 0.000842s 0.000063s   1187.3  15876.5
rsa 1024 bits 0.003809s 0.000166s    262.5   6013.2
rsa 2048 bits 0.022217s 0.000539s     45.0   1853.8
rsa 3072 bits 0.063608s 0.001140s     15.7    877.0
rsa 4096 bits 0.140556s 0.001954s      7.1    511.7
rsa 7680 bits 0.800000s 0.006616s      1.2    151.1
rsa 15360 bits 5.870000s 0.025718s      0.2     38.9
                  sign    verify    sign/s verify/s
dsa  512 bits 0.001195s 0.000796s    836.6   1256.5
dsa 1024 bits 0.002489s 0.001960s    401.8    510.3
dsa 2048 bits 0.007077s 0.006259s    141.3    159.8
                              sign    verify    sign/s verify/s
 160 bits ecdsa (secp160r1)   0.0039s   0.0030s    259.4    328.9
 192 bits ecdsa (nistp192)   0.0056s   0.0042s    178.7    237.2
 224 bits ecdsa (nistp224)   0.0077s   0.0056s    129.7    177.9
 256 bits ecdsa (nistp256)   0.0008s   0.0022s   1331.4    447.0
 384 bits ecdsa (nistp384)   0.0276s   0.0189s     36.2     52.8
 521 bits ecdsa (nistp521)   0.0661s   0.0446s     15.1     22.4
 163 bits ecdsa (nistk163)   0.0042s   0.0084s    235.8    119.7
 233 bits ecdsa (nistk233)   0.0072s   0.0143s    138.5     69.7
 283 bits ecdsa (nistk283)   0.0128s   0.0252s     78.1     39.7
 409 bits ecdsa (nistk409)   0.0262s   0.0515s     38.2     19.4
 571 bits ecdsa (nistk571)   0.0603s   0.1185s     16.6      8.4
 163 bits ecdsa (nistb163)   0.0045s   0.0089s    223.4    112.6
 233 bits ecdsa (nistb233)   0.0077s   0.0153s    129.5     65.2
 283 bits ecdsa (nistb283)   0.0139s   0.0275s     71.7     36.4
 409 bits ecdsa (nistb409)   0.0290s   0.0574s     34.5     17.4
 571 bits ecdsa (nistb571)   0.0678s   0.1328s     14.8      7.5
 256 bits ecdsa (brainpoolP256r1)   0.0085s   0.0068s    117.5    148.0
 256 bits ecdsa (brainpoolP256t1)   0.0085s   0.0065s    117.7    153.8
 384 bits ecdsa (brainpoolP384r1)   0.0274s   0.0204s     36.5     49.0
 384 bits ecdsa (brainpoolP384t1)   0.0273s   0.0191s     36.6     52.4
 512 bits ecdsa (brainpoolP512r1)   0.0361s   0.0272s     27.7     36.8
 512 bits ecdsa (brainpoolP512t1)   0.0357s   0.0251s     28.0     39.9
                              op      op/s
 160 bits ecdh (secp160r1)   0.0036s    276.4
 192 bits ecdh (nistp192)   0.0053s    189.3
 224 bits ecdh (nistp224)   0.0073s    136.9
 256 bits ecdh (nistp256)   0.0016s    631.8
 384 bits ecdh (nistp384)   0.0262s     38.1
 521 bits ecdh (nistp521)   0.0630s     15.9
 163 bits ecdh (nistk163)   0.0040s    249.7
 233 bits ecdh (nistk233)   0.0069s    145.6
 283 bits ecdh (nistk283)   0.0122s     82.3
 409 bits ecdh (nistk409)   0.0248s     40.4
 571 bits ecdh (nistk571)   0.0569s     17.6
 163 bits ecdh (nistb163)   0.0043s    234.3
 233 bits ecdh (nistb233)   0.0074s    135.4
 283 bits ecdh (nistb283)   0.0133s     75.1
 409 bits ecdh (nistb409)   0.0277s     36.1
 571 bits ecdh (nistb571)   0.0640s     15.6
 256 bits ecdh (brainpoolP256r1)   0.0080s    124.7
 256 bits ecdh (brainpoolP256t1)   0.0080s    124.7
 384 bits ecdh (brainpoolP384r1)   0.0262s     38.2
 384 bits ecdh (brainpoolP384t1)   0.0261s     38.4
 512 bits ecdh (brainpoolP512r1)   0.0343s     29.2
 512 bits ecdh (brainpoolP512t1)   0.0341s     29.3
 253 bits ecdh (X25519)   0.0020s    497.6
 448 bits ecdh (X448)   0.0077s    129.9
                              sign    verify    sign/s verify/s
 253 bits EdDSA (Ed25519)   0.0008s   0.0022s   1293.7    445.6
 456 bits EdDSA (Ed448)   0.0044s   0.0085s    227.4    118.0
#+END_SRC

*** openvpn

#+BEGIN_SRC
root@bananapi-m2b:~# openvpn --genkey --secret /tmp/secret
root@bananapi-m2b:~# time openvpn --test-crypto --secret /tmp/secret --verb 0 --tun-mtu 20000 --cipher aes-256-cbc
Sun Dec 29 13:20:37 2019 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode

real    0m41.862s
user    0m41.807s
sys     0m0.031s
root@bananapi-m2b:~# time openvpn --test-crypto --secret /tmp/secret --verb 0 --tun-mtu 20000 --cipher aes-128-cbc
Sun Dec 29 13:21:23 2019 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode

real    0m36.297s
user    0m36.236s
sys     0m0.061s
root@bananapi-m2b:~# time openvpn --test-crypto --secret /tmp/secret --verb 0 --tun-mtu 20000 --cipher aes-128-gcm
Sun Dec 29 13:22:05 2019 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode

real    0m34.949s
user    0m34.908s
sys     0m0.041s
root@bananapi-m2b:~# time openvpn --test-crypto --secret /tmp/secret --verb 0 --tun-mtu 20000 --engine afalg --cipher aes-128-gcm
Sun Dec 29 13:22:48 2019 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode

real    0m35.140s
user    0m35.052s
sys     0m0.040s
root@bananapi-m2b:~# time openvpn --test-crypto --secret /tmp/secret --verb 0 --tun-mtu 20000 --engine afalg --cipher aes-128-cbc
Sun Dec 29 13:23:28 2019 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode

real    1m31.128s
user    0m21.858s
sys     0m2.858s
#+END_SRC
